package com.test.codethatkills.timesofindia.Items;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * class is used to parse JSON News object by Retrofit lib
 */
public class NewsItem implements Serializable{

    @SerializedName("NewsItemId")
    @Expose
    private String newsItemId;
    @SerializedName("HeadLine")
    @Expose
    private String headLine;
    @SerializedName("Agency")
    @Expose
    private String agency;
    @SerializedName("DateLine")
    @Expose
    private String dateLine;
    @SerializedName("WebURL")
    @Expose
    private String webURL;
    @SerializedName("Caption")
    @Expose
    private String caption;
    @SerializedName("Image")
    @Expose
    private NewsImage image;
    @SerializedName("Keywords")
    @Expose
    private String keywords;
    @SerializedName("Story")
    @Expose
    private String story;
    @SerializedName("CommentCountUrl")
    @Expose
    private String commentCountUrl;
    @SerializedName("CommentFeedUrl")
    @Expose
    private String commentFeedUrl;
    @SerializedName("Related")
    @Expose
    private String related;

    public String getNewsItemId() {
        return newsItemId;
    }

    public String getHeadLine() {
        return headLine;
    }

    public String getDateLine() {
        return dateLine;
    }

    public String getWebURL() {
        return webURL;
    }

    public String getCaption() {
        return caption;
    }

    public NewsImage getImage() {
        return image;
    }

}