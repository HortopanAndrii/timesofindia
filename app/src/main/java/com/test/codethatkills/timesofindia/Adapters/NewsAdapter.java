package com.test.codethatkills.timesofindia.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.test.codethatkills.timesofindia.Items.NewsImage;
import com.test.codethatkills.timesofindia.Items.NewsItem;
import com.test.codethatkills.timesofindia.R;

import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {
    private List<NewsItem> mData = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context mContext;
    private IOnNewsItemSelectedListener mListener;
    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            NewsViewHolder holder = (NewsViewHolder) view.getTag();
            int position = holder.getAdapterPosition();

            mListener.onNewsItemSelected(mData.get(position));
        }
    };

    public NewsAdapter(Context context, IOnNewsItemSelectedListener listener) {
        mInflater = LayoutInflater.from(context);
        mListener = listener;
        this.mContext = context;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.news_row, parent, false);
        NewsViewHolder holder = new NewsViewHolder(view);
        view.setTag(holder);
        view.setOnClickListener(mClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        NewsItem current = mData.get(position);
        NewsImage image = current.getImage();
        if (image != null){
            String imageUrl = image.getThumb();
            Glide.with(mContext)
                    .load(imageUrl)
                    .into(holder.mThumb);
        } else {
            holder.mThumb.setImageResource(R.drawable.default_image);
        }
        holder.mTitle.setText(current.getHeadLine());
        holder.mCaption.setText(current.getCaption());
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    public void swapData(List<NewsItem> items) {
        this.mData = items;
        notifyDataSetChanged();
    }

    public NewsItem getItem(int position) {
        return mData == null ? null : mData.get(position);
    }

    public void add(NewsItem item) {
        mData.add(item);
        notifyDataSetChanged();
    }

    class NewsViewHolder extends RecyclerView.ViewHolder {
        private TextView mTitle;
        private TextView mCaption;
        private ImageView mThumb;

        public NewsViewHolder(View itemView) {
            super(itemView);
            mTitle = (TextView) itemView.findViewById(R.id.titleTextView);
            mCaption = (TextView) itemView.findViewById(R.id.captionTextView);
            mThumb = (ImageView) itemView.findViewById(R.id.thumb);
        }
    }

    /**
     * notifies according listener that news item was selected
     */
    public interface IOnNewsItemSelectedListener {
        void onNewsItemSelected(NewsItem newsItem);
    }
}