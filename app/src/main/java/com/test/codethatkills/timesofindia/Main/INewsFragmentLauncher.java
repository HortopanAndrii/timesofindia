package com.test.codethatkills.timesofindia.Main;

import com.test.codethatkills.timesofindia.Items.NewsItem;

public interface INewsFragmentLauncher {
    void launchDetailsFragment(NewsItem newsItem);
}
