package com.test.codethatkills.timesofindia.Items;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * class is used to parse JSON Image object by Retrofit lib
 */
public class NewsImage implements Serializable{

    @SerializedName("Photo")
    @Expose
    private String photo;
    @SerializedName("Thumb")
    @Expose
    private String thumb;
    @SerializedName("PhotoCaption")
    @Expose
    private String photoCaption;


    public String getThumb() {
        return thumb;
    }
}