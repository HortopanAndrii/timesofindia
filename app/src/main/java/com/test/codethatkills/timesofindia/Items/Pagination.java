package com.test.codethatkills.timesofindia.Items;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * class is used to parse JSON pagination object by Retrofit lib
 */
public class Pagination implements Serializable{

    @SerializedName("TotalPages")
    @Expose
    private String totalPages;
    @SerializedName("PageNo")
    @Expose
    private String pageNo;
    @SerializedName("PerPage")
    @Expose
    private String perPage;
    @SerializedName("WebURL")
    @Expose
    private String webUrl;

}