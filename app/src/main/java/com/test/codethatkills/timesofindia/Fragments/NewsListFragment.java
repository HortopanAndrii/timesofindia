package com.test.codethatkills.timesofindia.Fragments;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.test.codethatkills.timesofindia.Adapters.NewsAdapter;
import com.test.codethatkills.timesofindia.Items.NewsItem;
import com.test.codethatkills.timesofindia.Items.ResultItem;
import com.test.codethatkills.timesofindia.Main.INewsFragmentLauncher;
import com.test.codethatkills.timesofindia.Network.IApiMethods;
import com.test.codethatkills.timesofindia.R;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class NewsListFragment extends Fragment implements NewsAdapter.IOnNewsItemSelectedListener {

    private static final String TAG = "NewsListFragment";
    private NewsAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProgressBar mProgressBar;
    private TextView mNoConnectionTextView;

    public static NewsListFragment newInstance(){
        return new NewsListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                getNews();
            }
        });
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        mNoConnectionTextView = (TextView) rootView.findViewById(R.id.noConnectionTextView);
        mAdapter = new NewsAdapter(getActivity(), this);
        RecyclerView mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        getNews();
        Log.d(TAG, "onCreateView");
        return rootView;
    }

    private void getNews(){
        if (isDeviceOnline()){
            mNoConnectionTextView.setVisibility(View.GONE);
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(IApiMethods.API_URL)
                    .build();
            IApiMethods methods = restAdapter.create(IApiMethods.class);
            methods.getNews(new Callback<ResultItem>() {
                @Override
                public void success(ResultItem resultItem, Response response) {
                    mAdapter.swapData(resultItem.getNewsItems());
                    mSwipeRefreshLayout.setRefreshing(false);
                    mProgressBar.setVisibility(View.GONE);
                }

                @Override
                public void failure(RetrofitError error) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    mProgressBar.setVisibility(View.GONE);
                    Log.d(TAG, error.getMessage());
                }
            });
        } else {
            mAdapter.swapData(new ArrayList<NewsItem>());
            mNoConnectionTextView.setVisibility(View.VISIBLE);
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onNewsItemSelected(NewsItem newsItem) {
        Activity activity = getActivity();
        if(isDeviceOnline()){
            INewsFragmentLauncher launcher = (INewsFragmentLauncher)activity;
            launcher.launchDetailsFragment(newsItem);
        } else {
            Toast.makeText(getActivity(),
                    getString(R.string.offline_toast), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isDeviceOnline(){
        ConnectivityManager connMgr = (ConnectivityManager)getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
