package com.test.codethatkills.timesofindia.Network;

import com.test.codethatkills.timesofindia.Items.ResultItem;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * make requests using Retrofit lib
 */
public interface IApiMethods {
    String API_URL = "http://timesofindia.indiatimes.com";

    /**
     * make GET requests and return callback
     * if success callback return ResultItem
     * else RetrofitError
     */
    @GET("/feeds/newsdefaultfeeds.cms?feedtype=sjson")
    void getNews(
            Callback<ResultItem> cb
    );
}
