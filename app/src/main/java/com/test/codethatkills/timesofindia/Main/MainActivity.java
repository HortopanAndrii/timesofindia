package com.test.codethatkills.timesofindia.Main;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.test.codethatkills.timesofindia.Fragments.DetailsFragment;
import com.test.codethatkills.timesofindia.Fragments.NewsListFragment;
import com.test.codethatkills.timesofindia.Items.NewsItem;
import com.test.codethatkills.timesofindia.R;

public class MainActivity extends AppCompatActivity implements INewsFragmentLauncher {

    private final String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null){
            NewsListFragment fragment = NewsListFragment.newInstance();
            final String TAG = fragment.getTag();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction()
                    .replace(R.id.container, fragment, TAG)
                    .commit();
        }
        Log.d(TAG, "onCreate");
    }

    @Override
    public void launchDetailsFragment(NewsItem newsItem) {
        DetailsFragment fragment = DetailsFragment.newInstance(newsItem);
        final String TAG = fragment.getClass().getSimpleName();
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction()
                .add(R.id.container, fragment, TAG)
                .addToBackStack(TAG)
                .commit();
    }
}
