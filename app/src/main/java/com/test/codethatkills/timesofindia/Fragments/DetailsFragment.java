package com.test.codethatkills.timesofindia.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.test.codethatkills.timesofindia.Items.NewsItem;
import com.test.codethatkills.timesofindia.R;

public class DetailsFragment extends Fragment {

    private static final String TAG = "DetailsFragment";
    private static final String NEWS_KEY = "news";
    private NewsItem mCurrentNews;
    private ProgressBar mProgressBar;

    public static DetailsFragment newInstance(NewsItem item){
        DetailsFragment fragment = new DetailsFragment();
        if (item != null){
            Bundle args = new Bundle();
            args.putSerializable(NEWS_KEY, item);
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if(args != null){
            mCurrentNews = (NewsItem) args.getSerializable(NEWS_KEY);
        }
        Log.d(TAG, "onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.news_details_fragment, container, false);
        WebView mWebView = (WebView) rootView.findViewById(R.id.webView);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.detProgressBar);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                mProgressBar.setProgress(progress);
                if (progress == 100) {
                    mProgressBar.setVisibility(View.GONE);

                } else {
                    mProgressBar.setVisibility(View.VISIBLE);

                }
            }
        });
        mWebView.loadUrl(mCurrentNews.getWebURL());
        Log.d(TAG, "onCreateView");
        return rootView;
    }
}
